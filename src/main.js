import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import BaseCard from './components/base/BaseCard.vue'
import BaseError from './components/base/BaseError.vue'
import BaseDialog from './components/base/BaseDialog.vue'
import BaseAlert from './components/base/BaseAlert.vue'

Vue.config.productionTip = false

Vue.filter('unixTimeToLocalDate', function (unixTimestamp) {
  const date = new Date(unixTimestamp * 1000);
  const options = { weekday: 'short', year: '2-digit', month: 'numeric', day: 'numeric',
  hour: '2-digit', minute: '2-digit', hour24: 'true' }
  return date.toLocaleString('en-GB', options)
});

Vue.filter('unixTimeToLocalHourMinutes', function (unixTimestamp) {
  const date = new Date(unixTimestamp * 1000);
  const options = { hour: 'numeric', minute: 'numeric', hour24: 'true' }
  return date.toLocaleTimeString('en-GB', options)
});

Vue.component('BaseCard', BaseCard)
Vue.component('BaseError', BaseError)
Vue.component('BaseDialog', BaseDialog)
Vue.component('BaseAlert', BaseAlert)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
