import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
          dark: {
            darkback: '#21242b',
            primary: '#474c5c',
            secondary: '#6a9c99',
            accent: '#f29b82', //#ffaa71
            middle: '#f2bfb1',
            upper: '#addcd2'
          },
        },
      },
});
