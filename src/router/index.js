import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from '../pages/UserAuth.vue'
// import Home from '../pages/Home.vue'
// import Profiles from '../pages/profiles/ProfilesList.vue'
// import Rules from '../pages/profiles/ProfileDetails.vue'
// import Items from '../pages/items/ItemsList.vue'

const Home = () => import('../pages/Home.vue');
const Profiles = () => import('../pages/profiles/ProfilesList.vue');
const Rules = () => import('../pages/profiles/ProfileDetails.vue');
const Items = () => import('../pages/items/ItemsList.vue');
const UserSettings = () => import('../pages/UserSettings.vue');

Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/items', name: 'Items', component: Items },
  { path: '/profiles', name: 'Profiles', component: Profiles},
  { path: '/profiles/:profileId', name: 'Profile rules', component: Rules},
  { path: '/settings', name: 'Settings', component: UserSettings},
  { path: '/auth', name: 'Login', component: Auth},

  { path: '*', redirect: '/' }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, _, next) => {
  const idTokenExpiration = localStorage.getItem('tokenExpiration');
  const isLoggedIn = +idTokenExpiration - new Date().getTime() > 0;
  const toLoginPage = to.path === '/auth';

  if (toLoginPage && isLoggedIn) {
    next('/');
  } else if (!toLoginPage && !isLoggedIn) {
    next('/auth');
  } else {
    next();
  }
})

router.afterEach((to) => {
  document.title = to.name + ' | Dress4Weather';
});

export default router
