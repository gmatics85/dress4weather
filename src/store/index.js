import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import weatherModule from './modules/weather/index.js';
import profileModule from './modules/profiles/index.js';
import authModule from './modules/auth/index';

export default new Vuex.Store({
  modules: {
    weather: weatherModule,
    profiles: profileModule,
    auth: authModule,
    },
    state() {
      return {
        appError: null,
        errorCount: 0,
        version: 'v1.5'
      }
    },
    getters: {
      appErrors(state) {
        return state.appError;
      },
      version(state) {
        return state.version;
      }
    },
    mutations: {
      setAppError(state, payload) {
        state.errorCount++;
        state.appError = payload.message + ` (${state.errorCount})`;
      }
    } 
})
