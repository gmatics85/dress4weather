export default {
  setUser(state, payload) {
    state.token = payload.token;
    state.userId = payload.userId;
    state.isLoggedIn = payload.isLoggedIn;
    state.didAutoLogout = false;
  },
  setAutoLogout(state) {
    state.didAutoLogout = true;
  },
  setIsAutoLogin(state, payload) {
    state.isAutoLogin = payload.value;
  }
};