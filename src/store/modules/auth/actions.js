let timer;
const refreshCushion = 300000; // 5 mins cushion

export default {
  async auth(context, payload) {
    let url =
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAn25W6EeNMxXZx5OsrnqmDB0T7V6BS_T0';

    if (!payload.isLoginMode) {
      url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAn25W6EeNMxXZx5OsrnqmDB0T7V6BS_T0';
    }
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        email: payload.email,
        password: payload.password,
        returnSecureToken: true
      })
    });

    const responseData = await response.json();

    if (!response.ok) {
      let message = 'Failed to authenticate. Check your login data.'
      if (responseData.error.message === 'EMAIL_EXISTS') {
        message = 'Already registered. Try signing in!'
      }
      context.commit('setIsAutoLogin', { value: false });
      context.commit('setAppError', { message })
      return;
    }

    context.dispatch('setUserAndLocalStorage', {
      rememberMe: payload.rememberMe,
      ...responseData
    });
  },
  async tryLogin(context) {
    context.commit('setIsAutoLogin', { value: true });

    const userId = localStorage.getItem('userId');
    const token = localStorage.getItem('token');
    const tokenExpiration = localStorage.getItem('tokenExpiration');
    const refreshToken = localStorage.getItem('refreshToken');
    const expiresIn = +tokenExpiration - new Date().getTime();
    const isTokenValid = expiresIn - refreshCushion > 0;

    if (isTokenValid) {
      context.commit('setUser', {
        token,
        userId,
        isLoggedIn: true,
      });
      context.dispatch('startTimer', { autoLogout: !refreshToken, expiresIn });
      
    } else if (!isTokenValid && refreshToken) {
      context.dispatch('refreshToken');

    } else if (!isTokenValid && !refreshToken) {
      context.commit('setIsAutoLogin', { value: false });
      context.dispatch('autoLogout');
    }
  },
  async refreshToken(context) {
    const refreshToken = localStorage.getItem('refreshToken');
    let url = 'https://securetoken.googleapis.com/v1/token?key=AIzaSyAn25W6EeNMxXZx5OsrnqmDB0T7V6BS_T0';

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: `grant_type=refresh_token&refresh_token=${refreshToken}`
    });

    const responseData = await response.json();
    if (!response.ok) {
      context.commit('setIsAutoLogin', { value: false });
      context.commit('setAppError', {
        message: responseData.error.message || 'Failed to log you in automatically, please sign in.'
      })
      return;
    }

    context.dispatch('setUserAndLocalStorage', {
      rememberMe: true,
      idToken: responseData.id_token,
      localId: responseData.user_id,
      expiresIn: responseData.expires_in,
      refreshToken: responseData.refresh_token,
    });
  },
  setUserAndLocalStorage(context, payload) {
    const expiresIn = +payload.expiresIn * 1000;
    // const expiresIn = 10000; // testing
    const expirationDate = new Date().getTime() + expiresIn;

    localStorage.setItem('token', payload.idToken);
    localStorage.setItem('userId', payload.localId);
    localStorage.setItem('tokenExpiration', expirationDate);

    if (payload.rememberMe) {
      localStorage.setItem('refreshToken', payload.refreshToken);
    }

    context.commit('setUser', {
      token: payload.idToken,
      userId: payload.localId,
      isLoggedIn: true,
    });

    context.commit('setIsAutoLogin', { value: false });
    context.dispatch('startTimer', { autoLogout: !payload.rememberMe, expiresIn })
  },
  startTimer(context, payload) {
    clearTimeout(timer);

    if (payload.autoLogout) {

      timer = setTimeout(function() {
        context.dispatch('autoLogout');
      }, payload.expiresIn);
    } else {

      timer = setTimeout(function() {
        context.dispatch('tryLogin');
      }, payload.expiresIn - refreshCushion);
    }
  },
  logout(context) {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('tokenExpiration');
    localStorage.removeItem('refreshToken');

    clearTimeout(timer);

    context.dispatch('profiles/resetProfileData', null, { root: true });
    context.commit('setUser', {
      token: null,
      userId: null,
      isLoggedIn: false,
    });
    context.commit('setIsAutoLogin', { value: false });
  },
  autoLogout(context) {
    context.dispatch('logout');
    context.commit('setAutoLogout');
  }
};
