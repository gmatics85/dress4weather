export default {
  userId(state) {
    return state.userId;
  },
  token(state) {
    return state.token;
  },
  isLoggedIn(state) {
    return state.isLoggedIn;
  },
  isAutoLogin(state) {
    return state.isAutoLogin;
  },
  didAutoLogout(state) {
    return state.didAutoLogout;
  }
};