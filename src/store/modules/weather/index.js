import mutations from '../weather/mutations.js';
import actions from '../weather/actions.js';
import getters from '../weather/getters.js';

export default {
  namespaced: true,
  state() {
    return {
      city: '',
      weather: {},
      apiKey: null,
      dataIsReady: false,
      geoLocErrorMsg: null,
      geoLocation: null,
      lastFetch: null,
      selectedPeriod: {},
      tabs: [
        {
          icon: 0,
          title: "Next 3 hours",
          days: 0,
          nextHourRange: 3,
          fromHour: 17,
          toHour: 19,
        },
        {
          icon: 4,
          title: "This PM",
          days: 0,
          nextHourRange: 0,
          fromHour: 16,
          toHour: 19,
        },
        {
          icon: 7,
          title: "Next AM",
          days: 1,
          nextHourRange: 0,
          fromHour: 7,
          toHour: 10,
        },
        {
          icon: 6,
          title: "Next PM",
          days: 1,
          nextHourRange: 0,
          fromHour: 16,
          toHour: 19,
        },
      ],
      icons: [
        "mdi-arrow-right-circle-outline",
        "mdi-weather-night",
        "mdi-white-balance-sunny",
        "mdi-moon-waning-crescent",
        "mdi-weather-sunset",
        "mdi-weather-night-partly-cloudy",
        "mdi-weather-sunset-down",
        "mdi-weather-sunset-up",
      ],
    }
  },
  mutations,
  actions,
  getters
}