const posExpireIn = 1800000 // 30 minutes

export default {
    loadWeatherWithGeoLoc(context) {
        context.dispatch('setDataIsReady', { value: false });
        const longitude = localStorage.getItem('lon');
        const latitude = localStorage.getItem('lat');
        const posExpiryDate = localStorage.getItem('posExpiryDate');
        const posExpired = +posExpiryDate - new Date().getTime() < 0;

        if (posExpired) {
            
            if (!('geolocation' in navigator)) {
              context.dispatch('setGeoLocationError', { errorMsg: 'Geo-location is not available.' });
              return;
            }
            navigator.geolocation.getCurrentPosition(
              (pos) => {
                const lon = pos.coords.longitude;
                const lat = pos.coords.latitude;

                const posExpirationDate = new Date().getTime() + posExpireIn;
                localStorage.setItem('lon', lon);
                localStorage.setItem('lat', lat);
                localStorage.setItem('posExpiryDate', posExpirationDate);
    
                context.dispatch('setGeoLocation', { lon, lat });
                context.dispatch('loadWeather');
              },
              (err) => {
                context.dispatch('setGeoLocationError', { errorMsg: err.message });
              }
            );
        } else {
            context.dispatch('setGeoLocation', { lon: longitude, lat: latitude });
            context.dispatch('loadWeather');
        }
      },
    async loadWeather(context) {
        const pos = context.getters.geoLocation;
        const lon = pos.lon;
        const lat = pos.lat;

        if (!context.getters.apiKey) {
            await context.dispatch('loadWeatherApiKey')
        }

        await Promise.all([
            context.dispatch('loadWeatherData', { lon, lat }),
            context.dispatch('loadCityName', { lon, lat }),
            context.dispatch('loadTabsData'),
        ]);
        
        context.commit('setFetchTimestamp');
        context.commit('setDataIsReady', { value: true });
    },
    async loadWeatherApiKey(context) {
        const token = context.rootGetters.token;

        const response = await fetch(
            `https://dress4weather.firebaseio.com/weatherApi.json?auth=` + token
        );
        
        const responseData = await response.json();

        if (!response.ok) {
            context.commit('setAppError', {
                message: responseData.error || 'There was a problem fetching the weather API key.'
            }, { root: true })
            return;
        }

        if (responseData) {
            context.commit('setWeatherApiKey', responseData);
        }
    },
    async loadWeatherData(context, payload) {
        const apiKey = context.getters.apiKey;

        const response = await fetch(
            `https://api.openweathermap.org/data/2.5/onecall?lat=${payload.lat}&lon=${payload.lon}&units=metric&exclude=minutely&APPID=${apiKey}`
            );
        
        const responseData = await response.json();
        if (!response.ok) {
            context.commit('setAppError', {
                message: `Failed to fetch weather data: ${responseData.message}` 
            }, { root: true })
            return;
        }

        context.commit('setWeather', responseData);
    },
    async loadCityName(context, payload) {
        const apiKey = context.getters.apiKey;

        const response = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?lat=${payload.lat}&lon=${payload.lon}&APPID=${apiKey}`
            );
        
        const responseData = await response.json();
        if (!response.ok) {
            context.commit('setAppError', {
                message: `Failed to fetch city name: ${responseData.message}` 
            }, { root: true })
            return;
        }

        context.commit('setCityName', responseData.name);
    },
    async loadTabsData(context) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;

        const response = await fetch(
            `https://dress4weather.firebaseio.com/tabs/${userId}.json?auth=` + token
        );
        
        const responseData = await response.json();

        if (!response.ok) {
            context.commit('setAppError', {
                message: responseData.error || 'There was a problem fetching your tabs.'
            }, { root: true })
            return;
        }

        if (responseData) {
            context.commit('setTabsData', responseData);
        }
    },
    async saveTabsData(context, payload) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const tabs = payload;

        const response = await fetch(
            `https://dress4weather.firebaseio.com/tabs/${userId}.json?auth=` + token,
            {
                method: "PUT",
                body: JSON.stringify(tabs),
            }
        );

        const responseData = await response.json();
        
        if (!response.ok) {
            context.commit('setAppError', {
                message: `Failed to save your tabs: ${responseData.error}` 
            }, { root: true })
            return;
        }

        context.commit('setTabsData', tabs);
    },
    setWeatherApiKey(context, payload) {
        context.commit('setWeatherApiKey', payload);
    },
    setDataIsReady(context, payload) {
        context.commit('setDataIsReady', { value: payload.value });
    },
    setGeoLocation(context, payload) {
        context.commit('setGeoLocation', payload);
    },
    setGeoLocationError(context, payload) {
        context.commit('setGeoLocationError', payload);
    },
    setSelectedPeriod(context, payload) {
        context.commit('setSelectedPeriod', payload);
    },
};