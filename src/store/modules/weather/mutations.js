export default {
    setWeather(state, payload) {
        state.weather = payload;
    },
    setWeatherApiKey(state, payload) {
        state.apiKey = payload.appId;
    },
    setFetchTimestamp(state) {
        state.lastFetch = new Date().getTime();
    },
    setDataIsReady(state, payload) {
        state.dataIsReady = payload.value;
    },
    setGeoLocationError(state, payload) {
        state.geoLocErrorMsg = payload.errorMsg;
    },
    setGeoLocation(state, payload) {
        state.geoLocation = {
            lon: payload.lon,
            lat: payload.lat,
        };
    },
    setSelectedPeriod(state, payload) {
        state.selectedPeriod = payload;
    },
    setTabsData(state, payload) {
        state.tabs = payload;
    },
    resetTabs(state) {
        state.tabs = [
            {
              icon: 0,
              title: "Next 3 hours",
              days: 0,
              nextHourRange: 3,
              fromHour: 17,
              toHour: 19,
            },
        ]
    },
    setCityName(state, payload) {
        state.city = payload;
    }
};