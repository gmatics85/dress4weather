export default {
    weather(state) {
        return state.weather;
    },
    city(state) {
        return state.city;
    },
    apiKey(state) {
        return state.apiKey;
    },
    weatherAlerts(state) {
        return state.weather.alerts;
    },
    weatherHourly(state) {
        return state.weather.hourly;
    },
    lastFetch(state) {
        return state.lastFetch;
    },
    dataIsReady(state) {
        return state.dataIsReady;
    },
    geoLocation(state) {
        return state.geoLocation;
    },
    geoLocationErrorMsg(state) {
        return state.geoLocErrorMsg;
    },
    geoLocationIsUnavailable(state) {
        return !state.geoLocation && state.geoLocErrorMsg;
    },
    tabs(state) {
        return state.tabs;
    },
    icons(state) {
        return state.icons;
    },
    selectedPeriod(state, getters) {
        const selectedHours = [];
        let from, to = 0;
        let title = '';

        if (state.selectedPeriod !== undefined 
            && state.selectedPeriod.nextHourRange === 0) {
            from = state.selectedPeriod.fromHour;
            to = state.selectedPeriod.toHour;
            title = state.selectedPeriod.title;
        } else if (state.selectedPeriod.nextHourRange > 0) {
            const nextHour = new Date().getHours() + 1;
            from = nextHour;
            to = nextHour; // only get the next hour's temp (almost current)
            title = 'Next hour';
        }
        
        for (let i = from; i < to + 1; i++) {
            selectedHours.push(i);
        }

        let unixTimeArray = [];
        let filteredData = [];

        unixTimeArray = selectedHours.map((item) => {
            let tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + state.selectedPeriod.days);
            return tomorrow.setHours(item, 0, 0, 0) / 1000;
        });

        filteredData = getters.weatherHourly.filter((item) =>
            unixTimeArray.includes(item.dt)
        );

        let tempArray = filteredData.map(item => item.temp);
        
        return {
            title,
            temp: Math.round(Math.min(...tempArray)),
        }
    }
};