import mutations from '../profiles/mutations.js';
import actions from '../profiles/actions.js';
import getters from '../profiles/getters.js';

export default {
  namespaced: true,
  state() {
    return {
      dataIsReady: false,
      lastFetch: null,
      profiles: [],
      items: {},
      rulesAndItems: {}
    }
  },
  mutations,
  actions,
  getters
}
