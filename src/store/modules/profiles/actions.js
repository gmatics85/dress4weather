export default {
    async saveItems(context, payload) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const items = payload.items;
        
        const itemsResponse = await fetch(
            `https://dress4weather.firebaseio.com/items/${userId}.json?auth=` + token,
            {
                method: "PUT",
                body: JSON.stringify(items),
            }
        );

        if (!itemsResponse.ok) {
            const responseData = await itemsResponse.json();
            context.commit('setAppError', {
                message: responseData.error || 'Failed to save your items.'
            }, { root: true })
            return;
        }

        context.commit('setItems', { items });
        context.dispatch('loadRules', { forceRefresh: true });
    },
    async saveRulesAndItems(context, payload) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const profileId = payload.profileId;
        const rules = payload.rules;
        const items = payload.items;

        const rulesResponse = await fetch(
            `https://dress4weather.firebaseio.com/rules/${userId}/${profileId}.json?auth=` + token,
            {
                method: "PUT",
                body: JSON.stringify(rules),
            }
        );

        if (!rulesResponse.ok) {
            const responseData = await rulesResponse.json();
            context.commit('setAppError', {
                message: responseData.error || 'Failed to save your rules.'
            }, { root: true })
            return;
        }

        await context.dispatch('saveItems', { items })
        context.commit('setRulesAndItems', {
            profileId: profileId,
            rules: rules,
        })
    },
    async saveProfile(context, payload) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const response = await fetch(
            `https://dress4weather.firebaseio.com/profiles/${userId}.json?auth=` + token,
            {
                method: "POST",
                body: JSON.stringify(payload),
            }
        );

        const responseData = await response.json();
        
        if (!response.ok) {
            context.commit('setAppError', {
                message: `Failed to save new profile: ${responseData.error}` 
            }, { root: true })
            return;
        }

        context.commit('saveProfile', { id: responseData.name, ...payload });
    },
    async deleteProfile(context, payload) {
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const profileId = payload.profileId;

        const pDeleteResponse = await fetch(
            `https://dress4weather.firebaseio.com/profiles/${userId}/${profileId}.json?auth=` + token,
            {
                method: "DELETE",
            }
        );

        const rDeleteResponse = await fetch(
            `https://dress4weather.firebaseio.com/rules/${userId}/${profileId}.json?auth=` + token,
            {
                method: "DELETE",
            }
        );

        if (!pDeleteResponse.ok || !rDeleteResponse.ok) {
            context.commit('setAppError', {
                message: 'Failed to delete some of the data from the database!' 
            }, { root: true })
            return;
        }

        context.commit('deleteProfile', { profileId })
    },
    async loadAllData(context, payload) {
        if (!payload.forceRefresh && context.getters.dataIsReady) {
            return;
        }

        context.commit('setDataIsReady', { value: false });

        await Promise.all([
            context.dispatch('loadProfiles', { forceRefresh: payload.forceRefresh }),
            context.dispatch('loadRulesAndItems', { forceRefresh: payload.forceRefresh })
        ]);

        context.commit('setDataIsReady', { value: true });
        context.commit('setFetchTimestamp');
    },
    async loadProfiles(context, payload) {
        if (!payload.forceRefresh && context.getters.dataIsReady) {
            return;
        }

        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        const response = await fetch(
            `https://dress4weather.firebaseio.com/profiles/${userId}.json?auth=` + token,
        );

        const responseData = await response.json();

        if (!response.ok) {
            context.commit('setAppError', {
                message: `Failed to fetch your profiles: ${responseData.error}` 
            }, { root: true })
            return;
        }

        if (!responseData) {
            return;
        }

        const profiles = [];
        for (const key in responseData) {
            const profile = {
                id: key,
                name: responseData[key].name,
                avatar: responseData[key].avatar,
            };
            profiles.push(profile);
        }

        context.commit('setProfiles', profiles);
    },
    async loadRules(context, payload) {
        if (!payload.forceRefresh && context.getters.dataIsReady) {
            return;
        }

        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        
        const rulesResponse = await fetch(
            `https://dress4weather.firebaseio.com/rules/${userId}.json?auth=` + token,
        );
        
        const rules = await rulesResponse.json();

        if (!rulesResponse.ok) {
            context.commit('setAppError', {
                message: `Failed to fetch your rules: ${rules.error}` 
            }, { root: true })
            return;
        }

        if (rules !== null) {
            const rulesArray = Object.entries(rules);

            rulesArray.forEach(profile => {
                context.commit('setRulesAndItems', {
                    profileId: profile[0],
                    rules: profile[1],
                })
            })
        }
    },
    async loadRulesAndItems(context, payload) {
        if (!payload.forceRefresh && context.getters.dataIsReady) {
            return;
        }

        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;
        
        const itemsResponse = await fetch(
            `https://dress4weather.firebaseio.com/items/${userId}.json?auth=` + token,
        );

        const items = await itemsResponse.json();

        if (!itemsResponse.ok) {
            context.commit('setAppError', {
                message: `Failed to fetch your items: ${items.error}` 
            }, { root: true })
            return;
        }

        if (items) {
            context.commit('setItems', { items });
        }

        await context.dispatch('loadRules', { forceRefresh: payload.forceRefresh })
    },
    resetProfileData(context) {
        context.commit('resetProfileData');
    }
};