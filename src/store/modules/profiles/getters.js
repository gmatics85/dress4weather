export default {
    profiles(state) {
        return state.profiles;
    },
    rulesAndItems(state) {
        return state.rulesAndItems;
    },
    items(state) {
        return state.items;
    },
    lastFetch(state) {
        return state.lastFetch;
    },
    dataIsReady(state) {
        return state.dataIsReady;
    },
};