export default {
    setProfiles(state, payload) {
        state.profiles = payload;
    },
    setRulesAndItems(state, payload) {
        const ruleDataArray = [];
        const profileId = payload.profileId;
        const allItems = state.items;
        
        let allItemsEntries = [];
        if (allItems !== undefined) {
            allItemsEntries = Object.entries(allItems);
        }

        const rulesOfProfile = Object.entries(payload.rules);

        rulesOfProfile.forEach(rule => {
            const ruleItemKeys = Object.keys(rule[1].items)
            const ruleItems = allItemsEntries.filter(i => ruleItemKeys.includes(i[0]))

            const flattenedRuleItems = [];
            ruleItems.map(i => flattenedRuleItems.push({
                itemId: i[0],
                text: i[1].name,
                color: i[1].type
            })
            )

            flattenedRuleItems.sort((a, b) => {
                let ta = a.color.toLowerCase(),
                  tb = b.color.toLowerCase();
      
                if (ta < tb) {
                  return -1;
                }
                if (ta > tb) {
                  return 1;
                }
      
                return 0;
              });

            ruleDataArray.push({
                ruleId: rule[0],
                minTemp: rule[1].minTemp,
                maxTemp: rule[1].maxTemp,
                items: flattenedRuleItems,
            })
        })

        state.rulesAndItems[profileId] = ruleDataArray;
    },
    setItems(state, payload) {
        state.items = payload.items;
    },
    saveProfile(state, payload) {
        state.profiles.push(payload);
    },
    setFetchTimestamp(state) {
        state.lastFetch = new Date().getTime();
    },
    setDataIsReady(state, payload) {
        state.dataIsReady = payload.value;
    },
    deleteProfile(state, payload) {
        const pIndex = state.profiles.findIndex(p => p.id === payload.profileId);
        state.profiles.splice(pIndex, 1);
        state.rulesAndItems[payload.profileId] = [];
    },
    resetProfileData(state) {
        state.dataIsReady = false;
        state.lastFetch = null;
        state.profiles = [];
        state.items = {};
        state.rulesAndItems = {};
    }
};