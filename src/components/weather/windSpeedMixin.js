export default {
    data: () => ({
        windSpeedCats: [
            { upToSpeed: 11, name: 'Light' },
            { upToSpeed: 19, name: 'Gentle' },
            { upToSpeed: 28, name: 'Moderate' },
            { upToSpeed: 38, name: 'Fresh' },
            { upToSpeed: 61, name: 'Strong' },
            { upToSpeed: 88, name: 'Gale' },
            { upToSpeed: 117, name: 'Whole gale' },
            { upToSpeed: 999, name: 'Hurricane' },
        ]
    }),
    methods: {
        getWindSpeedCategory(windInMeterPerSeconds) {
            const windSpeedInKmph = Math.round(windInMeterPerSeconds * 3.6);
            const windSpeedCategory = this.windSpeedCats.find(cat => cat.upToSpeed >= windSpeedInKmph);

            if (!windSpeedCategory) {
                return ""
            } else {
                return windSpeedCategory.name;
            }
        }
    }
}