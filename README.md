# dress4weather

Dress up right for the weather!

As well as seeing next couple of hours forecast, you can also add your clothing items and create rules for what you want to wear based on temperature. For each customisable range of time (tabs) it can tell you what you should be wearing - based on the actual weather.

The tech behind it:
Vue JS + Vuetify
Firebase realtime database

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
